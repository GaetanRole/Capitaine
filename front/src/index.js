import 'babel-polyfill'
import 'bootstrap/dist/css/bootstrap.css'
import './css/font-awesome.min.css'
import './css/sb-admin.css'
import React from 'react'
import { push } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import injectTapEventPlugin from 'react-tap-event-plugin'
import {render} from 'react-dom'
import {Router, Route, IndexRoute, browserHistory, IndexRedirect} from 'react-router'
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import createLogger from 'redux-logger';
import rootReducer from './js/reducers'
import NotFound from './js/presentationals/NotFound.js'
import LoginContainer from './js/containers/LoginContainer.js'
import LogoutContainer from './js/containers/LogoutContainer.js'
import PlanesContainer from './js/containers/PlanesContainer.js'
import AddPlaneContainer from './js/containers/AddPlaneContainer.js'
import AirfieldsContainer from './js/containers/AirfieldsContainer.js'
import RoutesContainer from './js/containers/RoutesContainer.js'
import FlightPicturesContainer from './js/containers/FlightPicturesContainer.js'
import FlightsContainer from './js/containers/FlightsContainer.js'
import StatsFlightTimeContainer from './js/containers/StatsFlightTimeContainer.js'
import StatsFlightsContainer from './js/containers/StatsFlightsContainer.js'
import StatsOtherContainer from './js/containers/StatsOtherContainer.js'
import TrackingContainer from './js/containers/TrackingContainer.js'
import AeroContainer from './js/containers/AeroContainer.js'
import Header from './js/presentationals/Header.js'
import Sidebar from './js/presentationals/Sidebar.js'

// Needed for onTouchTap: http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();


const logger = createLogger();
const store = createStore(rootReducer, applyMiddleware(routerMiddleware(browserHistory), thunkMiddleware, logger));
const history = syncHistoryWithStore(browserHistory, store);


class Root extends React.Component {

    componentWillMount() {
        if (!(this.checkIfLogged())) {
            store.dispatch(push("/login"));
        }
    }

    checkIfLogged() {
        var token = JSON.parse(localStorage.getItem('token'));
        if (token) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <div className="body-page">
                <Header />
                <Sidebar />
                <div id="page-wrapper" className="page-wrapper">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/login" component={LoginContainer} />
            <Route path="/" component={Root}>
                <IndexRedirect to="/login" />
                <Route path="/logout" component={LogoutContainer} />
                <Route path="/planes" component={PlanesContainer} />
                <Route path="/addplane" component={AddPlaneContainer} />
                <Route path="/airfields/:action" component={AirfieldsContainer} />
                <Route path="/routes/:action" component={RoutesContainer} />
                <Route path="/flightpictures/:action" component={FlightPicturesContainer} />
                <Route path="/flights/:action" component={FlightsContainer} />
                <Route path="/stats/flighttime" component={StatsFlightTimeContainer} />
                <Route path="/stats/flights" component={StatsFlightsContainer} />
                <Route path="/stats/other" component={StatsOtherContainer} />
                <Route path="/aero/:action" component={AeroContainer} />
                <Route path="/tracking/:id" component={TrackingContainer} />
            </Route>
            <Route path="*" component={NotFound} />
        </Router>
    </Provider>,
    document.getElementById('root')
);

import "./css/app.css"
