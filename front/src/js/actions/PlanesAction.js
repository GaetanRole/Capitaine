import { jsonFetch } from '../utils/easyFetch.js'

export const GET_PLANES_START = 'GET_PLANES_START';
export const GET_PLANES_END = 'GET_PLANES_END';
export const GET_PLANES_ERROR = 'GET_PLANES_ERROR';

function getPlanesStart() {
    return {
        type: GET_PLANES_START
    }
}

function getPlanesEnd(planes) {
    return {
        type: GET_PLANES_END,
        planes
    }
}

function getPlanesError(error) {
    return {
        type: GET_PLANES_ERROR,
        error
    }
}

export function getPlanes(props) {
    return dispatch => {
        dispatch(getPlanesStart());
        setTimeout(function() {
            //Timeout de test mais inutile
            var token = JSON.parse(localStorage.getItem('token'));
            var headers = {
                Authorization: '' + token.token_type + ' ' + token.access_token,
            }
            console.log(headers);
            jsonFetch('http://149.202.169.91/api/planes/').setHeaders(headers).get().then(function(json) {
                console.log(json);
                dispatch(getPlanesEnd(json));
            }).catch(function(error) {
                console.log(error);
                dispatch(getPlanesError(error))
            });
        }, 2000);
    }
}