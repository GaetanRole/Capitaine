import { jsonFetch } from '../utils/easyFetch.js'

/*
** ADD ROUTES REGION
*/

export const ADD_ROUTE_START = 'ADD_ROUTE_START';
export const ADD_ROUTE_END = 'ADD_ROUTE_END';
export const ADD_ROUTE_ERROR = 'ADD_ROUTE_ERROR';

function addRouteStart() {
    return {
        type: ADD_ROUTE_START
    }
}

function addRouteEnd(routes) {
    return {
        type: ADD_ROUTE_END,
        routes
    }
}

function addRouteError(error) {
    return {
        type: ADD_ROUTE_ERROR,
        error
    }
}

export function addRoute(props) {
    return dispatch => {
        dispatch(addRouteStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        };
        var params = {
            name: props.name,
            steps: props.steps,
        };
        jsonFetch('http://149.202.169.91/api/routes/').setHeaders(headers).post(params).then(function(json) {
            console.log(json);
            dispatch(addRouteEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(addRouteError(error))
        });
    }
}

/*
** END REGION
*/

/*
** GET ROUTE REGION
*/

export const GET_ROUTES_START = 'GET_ROUTES_START';
export const GET_ROUTES_END = 'GET_ROUTES_END';
export const GET_ROUTES_ERROR = 'GET_ROUTES_ERROR';

function getRoutesStart() {
    return {
        type: GET_ROUTES_START
    }
}

function getRoutesEnd(routes) {
    return {
        type: GET_ROUTES_END,
        routes
    }
}

function getRoutesError(error) {
    return {
        type: GET_ROUTES_ERROR,
        error
    }
}

export function getRoutes(props) {
    return dispatch => {
        dispatch(getRoutesStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        }
        jsonFetch('http://149.202.169.91/api/routes/').setHeaders(headers).get().then(function(json) {
            console.log(json);
            dispatch(getRoutesEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(getRoutesError(error))
        });
    }
}

/*
** END REGION
*/