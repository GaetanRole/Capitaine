import { push } from 'react-router-redux'

export const LOGOUT_START = 'LOGOUT_START'
export const LOGOUT_END = 'LOGOUT_END'

function logoutStart() {
    return {
        type: LOGOUT_START
    }
}

function logoutEnd() {
    return {
        type: LOGOUT_END
    }
}

export function logout(props) {
    return dispatch => {
        dispatch(logoutStart());
        localStorage.removeItem('token');
        dispatch(logoutEnd());
        dispatch(push('/login'));
    }
}