import { jsonFetch } from '../utils/easyFetch.js'

export const ADD_PLANE_START = 'ADD_PLANE_START';
export const ADD_PLANE_END = 'ADD_PLANE_END';
export const ADD_PLANE_ERROR = 'ADD_PLANE_ERROR';

function addPlaneStart() {
    return {
        type: ADD_PLANE_START
    }
}

function addPlaneEnd(planes) {
    return {
        type: ADD_PLANE_END,
        planes
    }
}

function addPlaneError(error) {
    return {
        type: ADD_PLANE_ERROR,
        error
    }
}

export function addPlane(props) {
    return dispatch => {
        dispatch(addPlaneStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        };
        var params = {
            registration: props.registration,
            type: props.type,
            brand: props.brand,
        };
        console.log(headers);
        jsonFetch('http://149.202.169.91/api/planes/').setHeaders(headers).post(params).then(function(json) {
            console.log(json);
            dispatch(addPlaneEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(addPlaneError(error))
        });
    }
}