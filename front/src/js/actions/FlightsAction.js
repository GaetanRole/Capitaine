import { jsonFetch } from '../utils/easyFetch.js'

/*
** ADD FLIGHTS REGION
*/

export const ADD_FLIGHT_START = 'ADD_FLIGHT_START';
export const ADD_FLIGHT_END = 'ADD_FLIGHT_END';
export const ADD_FLIGHT_ERROR = 'ADD_FLIGHT_ERROR';

function addFlightStart() {
    return {
        type: ADD_FLIGHT_START
    }
}

function addFlightEnd(flights) {
    return {
        type: ADD_FLIGHT_END,
        flights
    }
}

function addFlightError(error) {
    return {
        type: ADD_FLIGHT_ERROR,
        error
    }
}

export function addFlight(props) {
    return dispatch => {
        dispatch(addFlightStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        };
        var params = {
        };
        jsonFetch('http://149.202.169.91/api/flights/').setHeaders(headers).post(params).then(function(json) {
            console.log(json);
            dispatch(addFlightEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(addFlightError(error))
        });
    }
}

/*
** END REGION
*/

/*
** GET FLIGHTS REGION
*/

export const GET_FLIGHTS_START = 'GET_FLIGHTS_START';
export const GET_FLIGHTS_END = 'GET_FLIGHTS_END';
export const GET_FLIGHTS_ERROR = 'GET_FLIGHTS_ERROR';

function getFlightsStart() {
    return {
        type: GET_FLIGHTS_START
    }
}

function getFlightsEnd(flights) {
    return {
        type: GET_FLIGHTS_END,
        flights
    }
}

function getFlightsError(error) {
    return {
        type: GET_FLIGHTS_ERROR,
        error
    }
}

export function getFlights(props) {
    return dispatch => {
        dispatch(getFlightsStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        }
        jsonFetch('http://149.202.169.91/api/flights/').setHeaders(headers).get().then(function(json) {
            console.log(json);
            dispatch(getFlightsEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(getFlightsError(error))
        });
    }
}

/*
** END REGION
*/

/*
** GET FLIGHT BY ID REGION
*/

export const GET_FLIGHTBYID_START = 'GET_FLIGHTBYID_START';
export const GET_FLIGHTBYID_END = 'GET_FLIGHTBYID_END';
export const GET_FLIGHTBYID_ERROR = 'GET_FLIGHTBYID_ERROR';

function getFlightByIdStart() {
    return {
        type: GET_FLIGHTBYID_START
    }
}

function getFlightByIdEnd(flight) {
    return {
        type: GET_FLIGHTBYID_END,
        flight
    }
}

function getFlightByIdError(error) {
    return {
        type: GET_FLIGHTBYID_ERROR,
        error
    }
}

export function getFlightById(id) {
    return dispatch => {
        dispatch(getFlightByIdStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        }
        jsonFetch('http://149.202.169.91/api/flights/' + id + '/').setHeaders(headers).get().then(function(json) {
            console.log(json);
            dispatch(getFlightByIdEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(getFlightByIdError(error))
        });
    }
}

/*
** END REGION
*/