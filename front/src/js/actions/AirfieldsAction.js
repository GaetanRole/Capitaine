import { jsonFetch } from '../utils/easyFetch.js'

/*
** ADD AIRFIELDS REGION
*/

export const ADD_AIRFIELD_START = 'ADD_AIRFIELD_START';
export const ADD_AIRFIELD_END = 'ADD_AIRFIELD_END';
export const ADD_AIRFIELD_ERROR = 'ADD_AIRFIELD_ERROR';

function addAirfieldStart() {
    return {
        type: ADD_AIRFIELD_START
    }
}

function addAirfieldEnd(airfields) {
    return {
        type: ADD_AIRFIELD_END,
        airfields
    }
}

function addAirfieldError(error) {
    return {
        type: ADD_AIRFIELD_ERROR,
        error
    }
}

export function addAirfield(props) {
    return dispatch => {
        dispatch(addAirfieldStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        };
        var params = {
            code: props.code,
            name: props.name,
            latitude: props.latitude,
            longitude: props.longitude,
        };
        jsonFetch('http://149.202.169.91/api/airfields/').setHeaders(headers).post(params).then(function(json) {
            console.log(json);
            dispatch(addAirfieldEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(addAirfieldError(error))
        });
    }
}

/*
** END REGION
*/

/*
** GET AIRFIELD REGION
*/

export const GET_AIRFIELDS_START = 'GET_AIRFIELDS_START';
export const GET_AIRFIELDS_END = 'GET_AIRFIELDS_END';
export const GET_AIRFIELDS_ERROR = 'GET_AIRFIELDS_ERROR';

function getAirfieldsStart() {
    return {
        type: GET_AIRFIELDS_START
    }
}

function getAirfieldsEnd(airfields) {
    return {
        type: GET_AIRFIELDS_END,
        airfields
    }
}

function getAirfieldsError(error) {
    return {
        type: GET_AIRFIELDS_ERROR,
        error
    }
}

export function getAirfields(props) {
    return dispatch => {
        dispatch(getAirfieldsStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        }
        jsonFetch('http://149.202.169.91/api/airfields/').setHeaders(headers).get().then(function(json) {
            console.log(json);
            dispatch(getAirfieldsEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(getAirfieldsError(error))
        });
    }
}

/*
** END REGION
*/