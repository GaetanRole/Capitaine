import { jsonFetch, easyFetch } from '../utils/easyFetch.js'

export const GET_METAR_START = 'GET_METAR_START';
export const GET_METAR_END = 'GET_METAR_END';
export const GET_METAR_ERROR = 'GET_METAR_ERROR';

function getMetarStart() {
    return {
        type: GET_METAR_START
    }
}

function getMetarEnd(info) {
    return {
        type: GET_METAR_END,
        info
    }
}

function getMetarError(error) {
    return {
        type: GET_METAR_ERROR,
        error
    }
}

export function getMetarData(props) {
    return dispatch => {
        const metarTafRegex = /<raw_text>([\s\S]*?)<\/raw_text>/;

        dispatch(getMetarStart());
        easyFetch('http://149.202.169.91/metar-taf-proxy/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=3&mostRecent=true&stationString=' + props.code)
        .get()
        .then(response => response.text()
            .then(data => {
                !data.match(metarTafRegex)
                ? dispatch(getMetarError({}))
                : dispatch(getMetarEnd(data.match(metarTafRegex)[1]));
            })
        )
        .catch(function(error) {
            console.log(error);
            dispatch(getMetarError(error))
        });
    }
}

export const GET_TAF_START = 'GET_TAF_START';
export const GET_TAF_END = 'GET_TAF_END';
export const GET_TAF_ERROR = 'GET_TAF_ERROR';

function getTafStart() {
    return {
        type: GET_TAF_START
    }
}

function getTafEnd(info) {
    return {
        type: GET_TAF_END,
        info
    }
}

function getTafError(error) {
    return {
        type: GET_TAF_ERROR,
        error
    }
}

export function getTafData(props) {
    return dispatch => {
        const metarTafRegex = /<raw_text>([\s\S]*?)<\/raw_text>/;

        dispatch(getTafStart());
        easyFetch('http://149.202.169.91/metar-taf-proxy/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&stationString=' + props.code + '&hoursBeforeNow=24&mostRecent=true')
        .get()
        .then(response => response.text()
            .then(data => {
                !data.match(metarTafRegex)
                ? dispatch(getTafError({}))
                : dispatch(getTafEnd(data.match(metarTafRegex)[1]));
            })
        )
        .catch(function(error) {
            console.log(error);
            dispatch(getTafError(error))
        });
    }
}

export const GET_NOTAM_START = 'GET_NOTAM_START';
export const GET_NOTAM_END = 'GET_NOTAM_END';
export const GET_NOTAM_ERROR = 'GET_NOTAM_ERROR';

function getNotamStart() {
    return {
        type: GET_NOTAM_START
    }
}

function getNotamEnd(info) {
    return {
        type: GET_NOTAM_END,
        info
    }
}

function getNotamError(error) {
    return {
        type: GET_NOTAM_ERROR,
        error
    }
}

export function getNotamData(props) {
    return dispatch => {
        dispatch(getNotamStart());
        easyFetch('http://149.202.169.91/notam-proxy/notamSearch/search')
        .setHeaders({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', Accept: 'application/json, text/plain'})
        .post('searchType=0&designatorsForLocation=' + props.code)
        .then(response => response.json()
            .then(data => {
                data.notamList.length === 0
                ? dispatch(getNotamError({}))
                : dispatch(getNotamEnd(data.notamList.reduce((a, b) => { return `${a}\n\n${b.icaoMessage}` }, data.notamList[0].icaoMessage)));
            })
        )
        .catch(function(error) {
            console.log(error);
            dispatch(getNotamError(error))
        });
    }
}

const login = 'capitaine';
const password = '9217a6db68cbe8247aa73e5da3a1d323';

export const GET_TEMSI_START = 'GET_TEMSI_START';
export const GET_TEMSI_END = 'GET_TEMSI_END';
export const GET_TEMSI_ERROR = 'GET_TEMSI_ERROR';

function getTemsiStart() {
    return {
        type: GET_TEMSI_START
    }
}

function getTemsiEnd(info) {
    return {
        type: GET_TEMSI_END,
        info
    }
}

function getTemsiError(error) {
    return {
        type: GET_TEMSI_ERROR,
        error
    }
}

export function getTemsiData(props) {
    return dispatch => {
        dispatch(getTemsiStart());
        easyFetch('http://149.202.169.91/temsi-proxy/ajax/login_valid.php')
        .setQueryParams({login: login, password: password})
        .post()
        .then(response => response.headers.get('set-cookies'))
        .then(cookies => cookies.match(/PHPSESSID=([a-z0-9]+)/gi)[1])
        .then(cookie =>
          easyFetch('http://149.202.169.91/temsi-proxy/affiche_image.php?type=sigwx/fr/france&date=' + props.date + '&mode=img')
            .setHeaders({Cookie: cookie})
            .get()
            .then(response => response.blob()
            .then(function(blob) {
                var url = URL.createObjectUrl(blob)
                console.log(url);
                dispatch(getTemsiEnd(url));
            }))
          )
        .catch(function(error) {
            console.log(error);
            dispatch(getTemsiError(error))
        });
    }
}

export const GET_WINTEM_START = 'GET_WINTEM_START';
export const GET_WINTEM_END = 'GET_WINTEM_END';
export const GET_WINTEM_ERROR = 'GET_WINTEM_ERROR';

function getWintemStart() {
    return {
        type: GET_WINTEM_START
    }
}

function getWintemEnd(info) {
    return {
        type: GET_WINTEM_END,
        info
    }
}

function getWintemError(error) {
    return {
        type: GET_WINTEM_ERROR,
        error
    }
}

export function getWintemData(props) {
    return dispatch => {
        dispatch(getWintemStart());
        easyFetch('http://149.202.169.91/temsi-proxy/ajax/login_valid.php')
        .setQueryParams({login: login, password: password})
        .post()
        .then(response => response.headers.get('set-cookies'))
        .then(cookies => cookies.match(/PHPSESSID=([a-z0-9]+)/gi)[1])
        .then(cookie =>
          easyFetch('http://149.202.169.91/temsi-proxy/affiche_image.php?type=wintemp/fr/france&date=' + props.date + '&mode=img')
            .setHeaders({Cookie: cookie})
            .get()
            .then(response => response.blob()
            .then(function(blob) {
                var url = URL.createObjectUrl(blob)
                console.log(url);
                dispatch(getWintemEnd(url));
            }))
          )
        .catch(function(error) {
            console.log(error);
            dispatch(getWintemError(error))
        });
    }
}
