import { jsonFetch } from '../utils/easyFetch'
import { push } from 'react-router-redux'

export const LOGIN_REQUEST_START = 'LOGIN_REQUEST_START';
export const LOGIN_REQUEST_END = 'LOGIN_REQUEST_END';
export const LOGIN_REQUEST_FAILED = 'LOGIN_REQUEST_FAILED';

function loginStarted() {
    return {
        type: LOGIN_REQUEST_START
    }
}

function loginEnded(infos) {
    return {
        type: LOGIN_REQUEST_END,
        infos
    }
}

function loginFailed(error) {
    return {
        type: LOGIN_REQUEST_FAILED,
        error
    }
}

export function getLoginToken(props) {
    return dispatch => {
        dispatch(loginStarted());
        var body = {
            client_id: '7ZZqrGLAUPSJsfELszLWqnqvyvtQCD4R3ekq2ibu',
            client_secret: 'EdRPOUJ6HTkjhlZIKT9qrGmW0JoAfYcNS5FmI691iIfDjdRwEi8T84A4khDaaciy6YWFiwnU73b0N0HxHhkq9nPeN8P10hgyjVdxT8A1oLctGRl8bpUk72gohIE7G0zx',
            grant_type: 'password',
            username: props.user,
            password: props.pass
        };
        jsonFetch('http://149.202.169.91/auth/token').setQueryParams(body).post().then(function(json) {
            console.log(json);
            localStorage.setItem('token', JSON.stringify(json));
            dispatch(loginEnded(json));
            dispatch(push("/planes"));
        }).catch(function(error) {
            console.log(error);
            dispatch(loginFailed(error))
        });
    }
}