import { jsonFetch } from '../utils/easyFetch.js'

/*
** ADD FLIGHTPICTURES REGION
*/

export const ADD_FLIGHTPICTURE_START = 'ADD_FLIGHTPICTURE_START';
export const ADD_FLIGHTPICTURE_END = 'ADD_FLIGHTPICTURE_END';
export const ADD_FLIGHTPICTURE_ERROR = 'ADD_FLIGHTPICTURE_ERROR';

function addFlightPictureStart() {
    return {
        type: ADD_FLIGHTPICTURE_START
    }
}

function addFlightPictureEnd(flightpictures) {
    return {
        type: ADD_FLIGHTPICTURE_END,
        flightpictures
    }
}

function addFlightPictureError(error) {
    return {
        type: ADD_FLIGHTPICTURE_ERROR,
        error
    }
}

export function addFlightPicture(props) {
    return dispatch => {
        dispatch(addFlightPictureStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        };
        var params = {
            image: props.image,
            description: props.description,
            flight: props.flight,
        };
        jsonFetch('http://149.202.169.91/api/flightpictures/').setHeaders(headers).post(params).then(function(json) {
            console.log(json);
            dispatch(addFlightPictureEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(addFlightPictureError(error))
        });
    }
}

/*
** END REGION
*/

/*
** GET FLIGHTPICTURE REGION
*/

export const GET_FLIGHTPICTURES_START = 'GET_FLIGHTPICTURES_START';
export const GET_FLIGHTPICTURES_END = 'GET_FLIGHTPICTURES_END';
export const GET_FLIGHTPICTURES_ERROR = 'GET_FLIGHTPICTURES_ERROR';

function getFlightPicturesStart() {
    return {
        type: GET_FLIGHTPICTURES_START
    }
}

function getFlightPicturesEnd(flightpictures) {
    return {
        type: GET_FLIGHTPICTURES_END,
        flightpictures
    }
}

function getFlightPicturesError(error) {
    return {
        type: GET_FLIGHTPICTURES_ERROR,
        error
    }
}

export function getFlightPictures(props) {
    return dispatch => {
        dispatch(getFlightPicturesStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        }
        jsonFetch('http://149.202.169.91/api/flightpictures/').setHeaders(headers).get().then(function(json) {
            console.log(json);
            dispatch(getFlightPicturesEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(getFlightPicturesError(error))
        });
    }
}

/*
** END REGION
*/