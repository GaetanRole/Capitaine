import { jsonFetch } from '../utils/easyFetch.js'

/*
** GET STATS REGION
*/

export const GET_STATS_START = 'GET_STATS_START';
export const GET_STATS_END = 'GET_STATS_END';
export const GET_STATS_ERROR = 'GET_STATS_ERROR';

function getStatsStart() {
    return {
        type: GET_STATS_START
    }
}

function getStatsEnd(stats) {
    return {
        type: GET_STATS_END,
        stats
    }
}

function getStatsError(error) {
    return {
        type: GET_STATS_ERROR,
        error
    }
}

export function getStats(props) {
    return dispatch => {
        dispatch(getStatsStart());
        var token = JSON.parse(localStorage.getItem('token'));
        var headers = {
            Authorization: '' + token.token_type + ' ' + token.access_token,
        }
        jsonFetch('http://149.202.169.91/api/stats/').setHeaders(headers).get().then(function(json) {
            console.log(json);
            dispatch(getStatsEnd(json));
        }).catch(function(error) {
            console.log(error);
            dispatch(getStatsError(error))
        });
    }
}

/*
** END REGION
*/