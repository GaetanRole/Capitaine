import React from 'react';
import { Grid, Col} from 'react-bootstrap';
import { Polar } from 'react-chartjs-2';

var AirfieldsLines = React.createClass({
    render: function() {
        var airfields = this.props.airfields;

        var formatted = airfields.map(function(airfields) {
            return (<td>{airfields.code} {airfields.name}</td>);
        });
        return (<table>{formatted}</table>);
    }
});

class StatsOther extends React.Component {

    componentWillMount() {
        this.props.dispatchGetStats();
    }

    render() {
        var div;
        if (this.props.statsother.requesting === false && this.props.statsother.response.success === true) {
            var stats = this.props.statsother.response.stats;
            div =
                <div>
                    <p><u>Temps de vol moyen par mois :</u> {stats.avg_flight_time_per_month / 60} heures soit {stats.avg_flight_time_per_month} minutes.</p>
                    <p><u>Coût total des vols :</u> {stats.total_flights_cost}.</p>
                    {/* nombre de vol par appareil */}
                    <p><u>Aérodromes visités :</u></p>
                    <AirfieldsLines airfields={stats.visited_airfields} />
                </div>
        }
        return (
            <Col>
                <div>
                    <h2 className="title">Statistiques du nombre de vols</h2>
                </div>
                <div className="main-page">
                    <div className="stat_chart">
                        <br />
                        {div}
                    </div>
                </div>
            </Col>
        )
    }
}

export default StatsOther
