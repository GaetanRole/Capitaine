import React from 'react';
import {
    Alert,
    Button,
    FormGroup,
    FormControl,
    Grid,
    Col
} from 'react-bootstrap';

class AddPlane extends React.Component {

    componentWillMount() {
    }

    constructor(props) {
        super(props);
        this.state = {
            registration: '',
            type: '',
            brand: '',
        };

        this.addPlaneHandler = this.addPlaneHandler.bind(this);
        this.handleRegistrationChange = this.handleRegistrationChange.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.handleBrandChange = this.handleBrandChange.bind(this);
    }

    addPlaneHandler(e) {
        e.preventDefault();
        this.props.dispatchAddPlane(this.state);
    }

    handleRegistrationChange(event) {
        this.setState({registration: event.target.value});
    }

    handleBrandChange(event) {
        this.setState({brand: event.target.value});
    }

    handleTypeChange(event) {
        this.setState({type: event.target.value});
    }

    render() {
        let result = null;
        if (this.props.addplane.response.success === true) {
            result = 
                <div>
                    <Alert bsStyle='success'>
                        Avion ajouté avec succès.
                    </Alert>
                </div>;
        } else if (this.props.addplane.response.error === true) {
            result = 
                <div>
                    <Alert bsStyle='danger'>
                        Erreur lors de l'ajout de cet avion.
                    </Alert>
                </div>;
        }

        return (
            <Grid>
                <Col md={6} mdOffset={3}>
                    AddPlane
                    {result}
                    <form onSubmit={this.addPlaneHandler}>
                        <fieldset className="formFont">
                            <FormGroup>
                                <FormControl type="text" componentClass="input" placeholder="Registration" value={this.state.registration} onChange={this.handleRegistrationChange} maxLength='10'/>
                            </FormGroup>
                            <FormGroup>
                                <FormControl componentClass="input" placeholder="Type" type="text" value={this.state.type} onChange={this.handleTypeChange} maxLength='50'/>
                            </FormGroup>
                            <FormGroup>
                                <FormControl componentClass="input" placeholder="Brand" type="text" value={this.state.brand} onChange={this.handleBrandChange} maxLength='50'/>
                            </FormGroup>
                            <Button type="submit" bsSize="large" bsStyle="primary" block>Ajouter l'avion</Button>
                        </fieldset>
                    </form>
                </Col>
            </Grid>
        )
    }
}

export default AddPlane
