import React from 'react';
import {
    Alert,
    Button,
    FormGroup,
    FormControl,
    Grid,
    Col
} from 'react-bootstrap';

class Tracking extends React.Component {

    componentWillMount() {
        if (this.props.params.id > 0) {
            this.props.dispatchGetFlightById(this.props.params.id);
        }
    }

    constructor(props) {
        super(props);

        this.getPathFromFlight = this.getPathFromFlight.bind(this);
    }

    getPathFromFlight() {
        var map = null;
        if (this.props.tracking.requesting === false) {
            if (this.props.tracking.response.success === true) {
                console.log(this.props.tracking.response.flight.path);
                var path = JSON.parse(this.props.tracking.response.flight.path);
                if (path.length === 1) {
                    map = 'http://maps.googleapis.com/maps/api/staticmap?&size=600x600&zoom=14';
                    map += '&markers=icon:https://chart.apis.google.com/chart?chst=d_map_pin_icon%26chld=airport%257CFFFFFF|' + path[0].latitude + ',' + path[0].longitude;
                    map += '&path=color:blue|weight:5';
                    path.forEach(function(elem) {
                        map += '|' + elem.latitude + ',' + elem.longitude;
                    });
                    map += '&key=AIzaSyC9JmpjAfSm6tLGNcy5SEa0hD-ZrDkJl8g';
                } else if (path.length > 1) {
                    map = 'http://maps.googleapis.com/maps/api/staticmap?&size=600x600&zoom=14';
                    map += '&markers=color:green|label:D|' + path[0].latitude + ',' + path[0].longitude;
                    map += '&markers=icon:https://chart.apis.google.com/chart?chst=d_map_pin_icon%26chld=airport%257CFFFFFF|' + path[path.length - 1].latitude + ',' + path[path.length - 1].longitude;
                    map += '&path=color:blue|weight:5';
                    path.forEach(function(elem) {
                        map += '|' + elem.latitude + ',' + elem.longitude;
                    });
                    map += '&key=AIzaSyC9JmpjAfSm6tLGNcy5SEa0hD-ZrDkJl8g';
                }
            }
        }
        return map;
    }

    render() {
        var link = this.getPathFromFlight()
        console.log(link);
        var imgMap;
        if (link) {
            imgMap = <img src={link} />
        }
        return (
            <Grid>
                {imgMap}
            </Grid>
            )
    }
}

export default Tracking
