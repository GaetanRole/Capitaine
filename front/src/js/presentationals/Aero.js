import React from 'react';
import moment from 'moment';
import { easyFetch } from '../utils/easyFetch'
import {
    Alert,
    Button,
    FormGroup,
    FormControl,
    Grid,
    Col
} from 'react-bootstrap';

let minTemsiDate = moment().format('YYYYMMDD090000');
let minWintemDate = moment().format('YYYYMMDD000000');

class Aero extends React.Component {

    componentWillMount() {
        var action = this.props.params.action;
        if (action === 'temsi') {
            if (moment().format('YYYYMMDDHHmmss') < minTemsiDate) {
              this.setState({temsiDate: moment().subtract(1, 'days').format('YYYYMMDD180000')})
            } else {
              while (parseInt(minTemsiDate, 10) < parseInt(moment().format('YYYYMMDD180001'), 10)) {
                if (this.state.temsiDate < moment().format('YYYYMMDDHHmmss')) {
                  this.setState({temsiDate: minTemsiDate})
                }
                minTemsiDate = parseInt(minTemsiDate, 10) + 30000
              }
            }
            while (parseInt(minWintemDate, 10) < parseInt(moment().format('YYYYMMDD220001'), 10)) {
              if (this.state.wintemDate < moment().format('YYYYMMDDHHmmss')) {
                this.setState({wintemDate: minWintemDate})
              }
              minWintemDate = parseInt(minWintemDate, 10) + 20000
            }
            this.props.dispatchGetWintemData({date: this.state.wintemDate});
            this.props.dispatchGetTemsiData({date: this.state.temsiDate});
        } else {
            this.props.dispatchGetAirfields();
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            wintemDate: '',
            temsiDate: '',
            airfield: '',
        }
        this.onAirfieldChange = this.onAirfieldChange.bind(this);
    }

    onAirfieldChange(event) {
        this.setState({airfield: event.target.value});
        console.log('onchange');
        console.log(this.props.params.action);
        if (this.props.params.action === 'metar') {
            this.props.dispatchGetMetarData({code: event.target.value});
        } else if (this.props.params.action === 'taf') {
            this.props.dispatchGetTafData({code: event.target.value});
        } else if (this.props.params.action === 'notam') {
            this.props.dispatchGetNotamData({code: event.target.value});
        }
    }

    render() {
        var select = <p></p>;
        var info = <p></p>;
        if (this.props.aero.requestingAirfields === false && this.props.aero.responseAirfields.success === true) {
            if (this.props.params.action === 'metar' || this.props.params.action === 'taf' || this.props.params.action === 'notam') {
                select = 
                    <select name="airfieldselect" onChange={this.onAirfieldChange}>
                        {this.props.aero.responseAirfields.data.map(function(item) {
                            return (<option value={item.code} key={item.code}>{item.code} - {item.name}</option>);
                        })}
                    </select>
            }
        }
        if (this.props.aero.requestingInfo === false && this.props.aero.responseInfo.success === true) {
            if (this.props.params.action === 'metar' || this.props.params.action === 'taf' || this.props.params.action === 'notam') {
                info = <p>{this.props.aero.responseInfo.data}</p>
            }
        }
        if (this.props.params.action === 'temsi') {
            if (this.props.aero.requestingInfo === false && this.props.aero.responseInfo.success === true) {
                info = 
                    <div>
                        <img src={this.props.aero.responseInfo.temsi} alt="temsi" />
                        <img src={this.props.aero.responseInfo.wintem} alt="wintem" />
                    </div>
            }
        }
        return (
            <Grid>
                {select}
                <br />
                {info}
            </Grid>
        )
    }
}

export default Aero
