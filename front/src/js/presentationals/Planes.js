import React from 'react';
import {
    Grid,
    Col
} from 'react-bootstrap';

class Planes extends React.Component {

    componentWillMount() {
        this.props.dispatchGetPlanes();
        document.body.style.backgroundImage = null;
        document.body.style.backgroundColor = "#f3f3f4";// Set the style
    }

    render() {
        var div;
        var requesting;
        if (this.props.planes.requesting === true) {
            requesting = <h1> Requete en cours </h1>
        } else {
            requesting = <h1> Requete terminée </h1>
        }
        if (this.props.planes.response.error === true) {
            div = <h1> ERRROR </h1>
        } else if (this.props.planes.response.success === true && this.props.planes.response.planes) {
            div = <div>
                <h1> OKKKK </h1>
                <p> {JSON.stringify(this.props.planes.response.planes)} </p>
            </div>
        }

        return (
            <Col className="main-page">
                getPlanes
                {requesting}
                {div}
            </Col>
        )
    }
}

export default Planes
