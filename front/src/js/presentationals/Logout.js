import React from 'react';
import {
    Button,
    Grid,
    Col
} from 'react-bootstrap';

class Logout extends React.Component {

    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout(event) {
        event.preventDefault();
        this.props.dispatchLogout();
    }

    render() {
        return (
            <Grid>
                <Col md={6} mdOffset={3}>
                    <div className="text-center">
                        Êtes-vous sur de vouloir vraiment vous deconnecter ?
                        <br />
                        <Button bsStyle="danger" onClick={this.logout}>Se deconnecter</Button>
                    </div>
                </Col>
            </Grid>
        )
    }
}

export default Logout
