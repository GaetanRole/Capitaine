import React from 'react';
import { Grid, Col } from 'react-bootstrap';
import { Pie, defaults } from 'react-chartjs-2';

require ("../../css/chart.css");

class StatsFlightTime extends React.Component {

    componentWillMount() {
        this.props.dispatchGetStats();
        this.state = {
            chart: 1
        };
        this.chartSelectChange = this.chartSelectChange.bind(this);
    }

    chartSelectChange(event) {
        this.setState({chart: Number(event.target.value)});
    }

    render() {
        var total_flight_chart;
        var since_license_chart;
        var last_year_chart;
        var last_3m_chart;
        console.log(JSON.stringify(this.props));
        if (this.props.statsflighttime.requesting === false && this.props.statsflighttime.response.success === true) {
            var stats = this.props.statsflighttime.response.stats;
            /*
             ** TEMPS DE VOL
             */
            // Depuis le premier vol
            var total_flight_data = {
                labels: [
                    "Double commande",
                    "Commandant de bord"
                ],
                datasets : [
                    {
                        data: [stats.total_flight_time_double, stats.total_flight_time_alone],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }
                ]
            };
            var total_flight_options = {
                legend: {
                    display: true,
                    labels: {
                        fontSize: 17
                    }
                },
                title: {
                    display: true,
                    text: "Temps de vol total depuis le premier vol (en minutes)",
                    fontSize: 25
                }
            };
            total_flight_chart = <Pie data={total_flight_data} options={total_flight_options}/>;
            // Depuis l’obtention de la licence
            var since_license_data = {
                labels: [
                    "Double commande",
                    "Commandant de bord"
                ],
                datasets : [
                    {
                        data: [stats.since_license_flight_time_double, stats.since_license_flight_time_alone],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }
                ]
            };
            var since_license_options = {
                legend: {
                    display: true,
                    labels: {
                        fontSize: 17
                    }
                },
                title: {
                    display: true,
                    text: "Temps de vol total depuis l'obtention de la licence (en minutes)",
                    fontSize: 25
                }
            };
            since_license_chart = <Pie data={since_license_data} options={since_license_options}/>;
            // Depuis la derniere année
            var last_year_data = {
                labels: [
                    "Double commande",
                    "Commandant de bord"
                ],
                datasets : [
                    {
                        data: [(stats.last_year_flight_time_double === null ? 0 : stats.last_year_flight_time_double), (stats.last_year_flight_time_alone === null ? 0 : stats.last_year_flight_time_alone)],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }
                ]
            };
            var last_year_options = {
                legend: {
                    display: true,
                    labels: {
                        fontSize: 17
                    }
                },
                title: {
                    display: true,
                    text: "Temps de vol total depuis la dernière année (en minutes)",
                    fontSize: 25
                }
            };
            last_year_chart = <Pie data={last_year_data} options={last_year_options}/>;
            // Ces 3 derniers mois
            var last_3m_data = {
                labels: [
                    "Double commande",
                    "Commandant de bord"
                ],
                datasets : [
                    {
                        data: [(stats.last_3m_flight_time_double === null ? 0 : stats.last_3m_flight_time_double), (stats.last_3m_flight_time_alone === null ? 0 : stats.last_3m_flight_time_alone)],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }
                ]
            };
            var last_3m_options = {
                legend: {
                    display: true,
                    labels: {
                        fontSize: 17
                    }
                },
                title: {
                    display: true,
                    text: "Temps de vol total de ces trois derniers mois (en minutes)",
                    fontSize: 25
                }
            };
            last_3m_chart = <Pie data={last_3m_data} options={last_3m_options} />
        }
        var chart;
        switch (this.state.chart) {
            case 1:
                chart = total_flight_chart;
                break;
            case 2:
                chart = since_license_chart;
                break;
            case 3:
                chart = last_year_chart;
                break;
            case 4:
                chart = last_3m_chart;
                break;
        }
        return (
            <Col>
                <div>
                    <h2 className="title">Statistiques des temps de vol</h2>
                </div>
                <div className="main-page">
                    <div className="stat_chart">
                        <select name="chartselect" onChange={this.chartSelectChange}>
                            <option value={1} defaultValue>Depuis le premier vol</option>
                            <option value={2}>Depuis l’obtention de la licence</option>
                            <option value={3}>Dans la dernière année</option>
                            <option value={4}>Ces 3 derniers mois</option>
                        </select>
                        <br />
                        {chart}
                    </div>
                </div>
            </Col>
        )
    }
}

export default StatsFlightTime
