import React from 'react';
import {
    Alert,
    Button,
    FormGroup,
    FormControl,
    Grid,
    Col
} from 'react-bootstrap';
import NotFound from './NotFound.js';

class Routes extends React.Component {

    componentWillMount() {
        if (this.props.params.action === 'show') {
            this.props.dispatchGetRoutes();
        }
    }

    constructor(props) {
        super(props);
        this.show = false;
        this.add = false;
        if (this.props.params.action === 'show') {
            this.show = true;
        }
        if (this.props.params.action === 'add') {
            this.add = true;
        }
        this.state = {
            name: '',
            steps: '',
        };

        this.addRouteHandler = this.addRouteHandler.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStepsChange = this.handleStepsChange.bind(this);
    }

    addRouteHandler(e) {
        e.preventDefault();
        this.props.dispatchAddRoute(this.state);
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleStepsChange(event) {
        this.setState({steps: event.target.value});
    }

    render() {
        /* Ajout d'une route */
        if (this.add) {
            let result = null;
            if (this.props.routes.response.success === true) {
                result = 
                    <div>
                        <Alert bsStyle='success'>
                            Route ajoutée avec succès.
                        </Alert>
                    </div>;
            } else if (this.props.routes.response.error === true) {
                result = 
                    <div>
                        <Alert bsStyle='danger'>
                            Erreur lors de l'ajout de cette route.
                        </Alert>
                    </div>;
            }

            return (
                <Grid>
                    <Col md={6} mdOffset={3}>
                        AddRoute
                        {result}
                        <form onSubmit={this.addRouteHandler}>
                            <fieldset className="formFont">
                                <FormGroup>
                                    <FormControl componentClass="input" placeholder="Name" type="text" value={this.state.name} onChange={this.handleNameChange} maxLength='100'/>
                                </FormGroup>
                                <FormGroup>
                                    <FormControl componentClass="textarea" placeholder="Steps" type="text" value={this.state.steps} onChange={this.handleStepsChange} />
                                </FormGroup>
                                <Button type="submit" bsSize="large" bsStyle="primary" block>Ajouter la route</Button>
                            </fieldset>
                        </form>
                    </Col>
                </Grid>
            )
        /* Affichage des routes*/
        } else if (this.show) {
            var div;
            if (this.props.routes.response.error === true) {
                div = <h1> ERRROR </h1>
            } else if (this.props.routes.response.success === true && this.props.routes.response.routes) {
                div = <div>
                    <h1> OKKKK </h1>
                    <p> {JSON.stringify(this.props.routes.response.routes)} </p>
                </div>
            }

            return (
                <Grid>
                    <Col md={6} mdOffset={3}>
                    getRoutes
                    {div}
                    </Col>
                </Grid>
            )
        /* 404 NotFound */
        } else {
            return (<NotFound />)
        }
    }
}

export default Routes
