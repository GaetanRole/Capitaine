import React from 'react';
import {
    Alert,
    Button,
    FormGroup,
    FormControl,
    Grid,
    Col
} from 'react-bootstrap';
import NotFound from './NotFound.js';

class FlightPictures extends React.Component {

    componentWillMount() {
        if (this.props.params.action === 'show') {
            this.props.dispatchGetFlightPictures();
        }
    }

    constructor(props) {
        super(props);
        this.show = false;
        this.add = false;
        if (this.props.params.action === 'show') {
            this.show = true;
        }
        if (this.props.params.action === 'add') {
            this.add = true;
        }
        this.state = {
            image: '',
            desc: '',
            latitude: '',
            longitude: '',
        };

        this.addFlightPictureHandler = this.addFlightPictureHandler.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLatitudeChange = this.handleLatitudeChange.bind(this);
        this.handleLongitudeChange = this.handleLongitudeChange.bind(this);
    }

    addFlightPictureHandler(e) {
        e.preventDefault();
        //this.props.dispatchAddFlightPicture(this.state);
        console.log(this.state.image);
    }

    handleImageChange(event) {
        this.setState({image: event.target.value});
    }

    handleDescriptionChange(event) {
        this.setState({desc: event.target.value});
    }

    handleLatitudeChange(event) {
        this.setState({latitude: event.target.value});
    }

    handleLongitudeChange(event) {
        this.setState({longitude: event.target.value});
    }

    render() {
        /* Ajout d'un aérodrome */
        if (this.add) {
            let result = null;
            if (this.props.flightpictures.response.success === true) {
                result = 
                    <div>
                        <Alert bsStyle='success'>
                            Image de vol ajouté avec succès.
                        </Alert>
                    </div>;
            } else if (this.props.flightpictures.response.error === true) {
                result = 
                    <div>
                        <Alert bsStyle='danger'>
                            Erreur lors de l'ajout de cet image de vol.
                        </Alert>
                    </div>;
            }

            return (
                <Grid>
                    <Col md={6} mdOffset={3}>
                        AddFlightPicture
                        {result}
                        <form onSubmit={this.addFlightPictureHandler}>
                            <fieldset className="formFont">
                                <FormGroup>
                                    <FormControl componentClass="input" type="file" value={this.state.image} onChange={this.handleImageChange}/>
                                </FormGroup>
                                <FormGroup>
                                    <FormControl componentClass="textarea" placeholder="Description" type="text" value={this.state.desc} onChange={this.handleDescriptionChange}/>
                                </FormGroup>
                                <Button type="submit" bsSize="large" bsStyle="primary" block>Ajouter l'image</Button>
                            </fieldset>
                        </form>
                    </Col>
                </Grid>
            )
        /* Affichage des aérodromes*/
        } else if (this.show) {
            var div;
            if (this.props.flightpictures.response.error === true) {
                div = <h1> ERRROR </h1>
            } else if (this.props.flightpictures.response.success === true && this.props.flightpictures.response.flightpictures) {
                div = <div>
                    <h1> OKKKK </h1>
                    <p> {JSON.stringify(this.props.flightpictures.response.flightpictures)} </p>
                    <img src={this.props.flightpictures.response.flightpictures[0].image} alt="" height="100" width="100" />
                </div>
            }

            return (
                <Grid>
                    <Col md={6} mdOffset={3}>
                    getFlightPictures
                    {div}
                    </Col>
                </Grid>
            )
        /* 404 NotFound */
        } else {
            return (<NotFound />)
        }
    }
}

export default FlightPictures
