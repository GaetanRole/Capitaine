import React from 'react';
import { Grid, Col } from 'react-bootstrap';
import { Bar, defaults } from 'react-chartjs-2';

class StatsFlights extends React.Component {

    componentWillMount() {
        this.props.dispatchGetStats();
    }

    render() {
        var chart;
        if (this.props.statsflights.requesting === false && this.props.statsflights.response.success === true) {
            var stats = this.props.statsflights.response.stats;
            /*
             ** TEMPS DE VOL
             */
            // Depuis le premier vol
            var total_flights_data = {
                labels: [
                    "Ces 3 derniers mois",
                    "Dans la dernière année",
                    "Depuis l'obtention de la licence",
                    "Depuis le premier vol"
                ],
                datasets : [
                    {
                        label: 'Nombre de vols',
                        data: [stats.last_3m_flights_nb, stats.last_year_flights_nb, stats.since_license_flights_nb, stats.flights_nb],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.5)',
                            'rgba(54, 162, 235, 0.5)',
                            'rgba(255, 206, 86, 0.5)',
                            'rgba(75, 192, 192, 0.5)'
                        ],
                        hoverBackgroundColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }
                ]
            };
            var total_flights_options = {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: "Nombre total de vols",
                    fontSize: 25
                }
            };
            chart = <Bar data={total_flights_data} options={total_flights_options}/>
        }
        return (
            <Col>
                <div>
                    <h2 className="title">Statistiques du nombre de vols</h2>
                </div>
                <div className="main-page">
                    <div className="stat_chart">
                        <br />
                        {chart}
                    </div>
                </div>
            </Col>
        )
    }
}

export default StatsFlights
