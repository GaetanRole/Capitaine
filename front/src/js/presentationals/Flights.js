import React from 'react';
import {
    Alert,
    Button,
    FormGroup,
    FormControl,
    Grid,
    Col
} from 'react-bootstrap';
import NotFound from './NotFound.js';

class Flights extends React.Component {

    componentWillMount() {
        if (this.props.params.action === 'show') {
            this.props.dispatchGetFlights();
        }
    }

    constructor(props) {
        super(props);
        this.show = false;
        this.add = false;
        if (this.props.params.action === 'show') {
            this.show = true;
        }
        if (this.props.params.action === 'add') {
            this.add = true;
        }
        this.state = {
            code: '',
            name: '',
            latitude: '',
            longitude: '',
        };

        this.addFlightHandler = this.addFlightHandler.bind(this);
        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleLatitudeChange = this.handleLatitudeChange.bind(this);
        this.handleLongitudeChange = this.handleLongitudeChange.bind(this);
    }

    addFlightHandler(e) {
        e.preventDefault();
        this.props.dispatchAddFlight(this.state);
    }

    handleCodeChange(event) {
        this.setState({code: event.target.value});
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleLatitudeChange(event) {
        this.setState({latitude: event.target.value});
    }

    handleLongitudeChange(event) {
        this.setState({longitude: event.target.value});
    }

    render() {
        /* Ajout d'un vol */
        if (this.add) {
            let result = null;
            if (this.props.flights.response.success === true) {
                result = 
                    <div>
                        <Alert bsStyle='success'>
                            Vol ajouté avec succès.
                        </Alert>
                    </div>;
            } else if (this.props.flights.response.error === true) {
                result = 
                    <div>
                        <Alert bsStyle='danger'>
                            Erreur lors de l'ajout de ce vol.
                        </Alert>
                    </div>;
            }

            return (
                <Grid>
                    <Col md={6} mdOffset={3}>
                        AddFlight
                        {result}
                        <form onSubmit={this.addFlightHandler}>
                            <fieldset className="formFont">
                                <FormGroup>
                                    <FormControl type="text" componentClass="input" placeholder="Code" value={this.state.code} onChange={this.handleCodeChange} maxLength='4'/>
                                </FormGroup>
                                <FormGroup>
                                    <FormControl componentClass="input" placeholder="Name" type="text" value={this.state.name} onChange={this.handleNameChange} maxLength='255'/>
                                </FormGroup>
                                <FormGroup>
                                    <FormControl componentClass="input" placeholder="Latitude" type="number" step="0.01" value={this.state.latitude} onChange={this.handleLatitudeChange} />
                                </FormGroup>
                                <FormGroup>
                                    <FormControl componentClass="input" placeholder="Longitude" type="number" step="0.01" value={this.state.longitude} onChange={this.handleLongitudeChange} />
                                </FormGroup>
                                <Button type="submit" bsSize="large" bsStyle="primary" block>Ajouter le vol</Button>
                            </fieldset>
                        </form>
                    </Col>
                </Grid>
            )
        /* Affichage des vols*/
        } else if (this.show) {
            var div;
            if (this.props.flights.response.error === true) {
                div = <h1> ERRROR </h1>
            } else if (this.props.flights.response.success === true && this.props.flights.response.flights) {
                div = <div>
                    <h1> OKKKK </h1>
                    <p> {JSON.stringify(this.props.flights.response.flights)} </p>
                </div>
            }

            return (
                <Grid>
                    <Col md={6} mdOffset={3}>
                    getFlights
                    {div}
                    </Col>
                </Grid>
            )
        /* 404 NotFound */
        } else {
            return (<NotFound />)
        }
    }
}

export default Flights
