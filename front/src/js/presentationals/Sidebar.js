import React from 'react';
import classNames from 'classnames';

import '../../css/sidebar.css'

class Sidebar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            statsPageCollapsed: true,
            aeroPageCollapsed: true,
        };

        this.statsPageOnClick = this.statsPageOnClick.bind(this);
        this.aeroPageOnClick = this.aeroPageOnClick.bind(this);
    }

    statsPageOnClick(e) {
        e.preventDefault();
        this.setState({
            statsPageCollapsed: !this.state.statsPageCollapsed
        });
        return false;
    }

    aeroPageOnClick(e) {
        e.preventDefault();
        this.setState({
            aeroPageCollapsed: !this.state.aeroPageCollapsed
        });
        return false;
    }

    render() {
        return (
            <div id="wrapper">
                <nav className="navbar navbar-default navbar-color sidebar " style={{ height: '100%' }} role="navigation" id="sidebar-wrapper">
                    <div className="sidebar navbar-collapse collapse">
                        <ul className="nav in" id="side-menu">

                            <li>
                                <a href="/planes"><i className="pull-right hidden-xs showopacity glyphicon glyphicon-book"></i>Carnet de vol</a>
                            </li>

                            <li className={classNames({ active: !this.state.statsPageCollapsed })}>
                                <a href="" onClick={this.statsPageOnClick}><i className="pull-right hidden-xs showopacity glyphicon glyphicon-stats"></i>Statistiques</a>
                                <ul className={classNames({
                                    'nav nav-second-level': true,
                                    collapse: this.state.statsPageCollapsed,})}>
                                    <li>
                                        <a href="/stats/flighttime">Temps de vol</a>
                                    </li>
                                    <li>
                                        <a href="/stats/flights">Nombre de vols</a>
                                    </li>
                                    <li>
                                        <a href="/stats/other">Autres</a>
                                    </li>
                                </ul>
                            </li>

                            <li className={classNames({ active: !this.state.aeroPageCollapsed })}>
                                <a href="" onClick={this.aeroPageOnClick}><i className="pull-right hidden-xs showopacity glyphicon glyphicon-plane"></i>Services aéronautiques</a>
                                <ul className={classNames({
                                    'nav nav-second-level': true,
                                    collapse: this.state.aeroPageCollapsed,})}>
                                    <li>
                                        <a href="/aero/metar">METAR</a>
                                    </li>
                                    <li>
                                        <a href="/aero/taf">TAF</a>
                                    </li>
                                    <li>
                                        <a href="/aero/temsi">TEMSI/WINTEM</a>
                                    </li>
                                    <li>
                                        <a href="/aero/notam">NOTAM</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i className="pull-right hidden-xs showopacity glyphicon glyphicon-play"></i>Revivre ses vols</a>
                            </li>

                            {/* Gestion des avions */}
                            {/*<li className={classNames({ active: !this.state.planesPageCollapsed })}>
                             <a href=""
                             onClick={this.planesPageOnClick}>
                             Gestion des avions <span className="fa arrow" />
                             </a>
                             <ul className={classNames({
                             'nav nav-second-level': true,
                             collapse: this.state.planesPageCollapsed,})}>
                             <li>
                             <a href="/planes">Voir mes avions</a>
                             </li>
                             <li>
                             <a href="/addplane">Ajouter un avion</a>
                             </li>
                             </ul>
                             </li>*/}

                            {/* Gestion des aérodromes */}
                            {/*<li className={classNames({ active: !this.state.airfieldsPageCollapsed })}>
                             <a href=""
                             onClick={this.airfieldsPageOnClick}>
                             Gestion des aérodromes <span className="fa arrow" />
                             </a>
                             <ul className={classNames({
                             'nav nav-second-level': true,
                             collapse: this.state.airfieldsPageCollapsed,})}>
                             <li>
                             <a href="/airfields/show">Voir les aérodromes</a>
                             </li>
                             <li>
                             <a href="/airfields/add">Ajouter un aérodrome</a>
                             </li>
                             </ul>
                             </li>*/}

                            {/* Gestion des routes */}
                            {/*<li className={classNames({ active: !this.state.routesPageCollapsed })}>
                             <a href=""
                             onClick={this.routesPageOnClick}>
                             Gestion des routes <span className="fa arrow" />
                             </a>
                             <ul className={classNames({
                             'nav nav-second-level': true,
                             collapse: this.state.routesPageCollapsed,})}>
                             <li>
                             <a href="/routes/show">Voir les routes</a>
                             </li>
                             <li>
                             <a href="/routes/add">Ajouter une route</a>
                             </li>
                             </ul>
                             </li>*/}

                            {/* Gestion des vols */}
                            {/*<li className={classNames({ active: !this.state.flightsPageCollapsed })}>
                             <a href=""
                             onClick={this.flightsPageOnClick}>
                             Gestion des vols <span className="fa arrow" />
                             </a>
                             <ul className={classNames({
                             'nav nav-second-level': true,
                             collapse: this.state.flightsPageCollapsed,})}>
                             <li>
                             <a href="/flights/show">Voir les vols</a>
                             </li>
                             <li>
                             <a href="/flights/add">Ajouter un vol</a>
                             </li>
                             </ul>
                             </li>*/}

                            {/* Gestion des images de vols */}
                            {/*<li className={classNames({ active: !this.state.flightPicturesPageCollapsed })}>
                             <a href=""
                             onClick={this.flightPicturesPageOnClick}>
                             Gestion des images de vols <span className="fa arrow" />
                             </a>
                             <ul className={classNames({
                             'nav nav-second-level': true,
                             collapse: this.state.flightPicturesPageCollapsed,})}>
                             <li>
                             <a href="/flightpictures/show">Voir les images de vols</a>
                             </li>
                             <li>
                             <a href="/flightpictures/add">Ajouter une image de vol</a>
                             </li>
                             </ul>
                             </li>*/}

                            <br /> <hr /> <br />

                            <li>
                                <a href="/logout"><i className="pull-right hidden-xs showopacity glyphicon glyphicon-off"></i>Se deconnecter</a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}


export default Sidebar; 