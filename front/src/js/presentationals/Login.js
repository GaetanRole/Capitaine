import React from 'react';
import { FormGroup,
    FormControl,
    Button,
    Panel,
    PageHeader,
    Grid,
    Col,
    Alert
} from 'react-bootstrap';

import Background from '../../ressources/Background_Connexion.jpg';

require("../../css/login.css");

class Login extends React.Component {

    componentWillMount(){
        document.body.style.background = `url(${Background}) center center no-repeat fixed`;
        document.body.style.backgroundSize = "cover";
    }

    constructor(props) {
        super(props);
        this.state = {
            user: '',
            pass: ''
        };

        this.handleUserChange = this.handleUserChange.bind(this);
        this.handlePassChange = this.handlePassChange.bind(this);
        this.loginSubmitHandler = this.loginSubmitHandler.bind(this);
    }

    loginSubmitHandler(event) {
        event.preventDefault();
        this.props.dispatchGetLoginToken(this.state);
    }

    handleUserChange(event) {
        this.setState({user: event.target.value});
    }

    handlePassChange(event) {
        this.setState({pass: event.target.value});
    }

    render() {
        let errorDiv = null;
        if (this.props.login.response.error === true) {
            errorDiv =
                <div>
                    <Alert bsStyle='danger'>
                        Problème de connexion. Veuillez vérifier votre login / mot de passe.
                    </Alert>
                </div>
        }

        return (
            <Grid>
                <Col md={6} mdOffset={3}>
                    <div className="text-center">
                        <PageHeader className="titleHeader">BIENVENUE SUR CAPITAINE</PageHeader>
                        <h3 className="textHeader"><span className="typoLogo">Capitaine</span> est une application gratuite destinée aux pilotes
                            <br></br> privés qui regroupe tous les outils nécessaires
                            <br></br> avant, pendant et après le vol.
                        </h3>
                    </div>

                    {errorDiv}

                    <Panel className="formOpacity formTitle" header={<h3>Connexion à mon compte</h3>}>
                        <form onSubmit={this.loginSubmitHandler}>
                            <fieldset className="formFont">
                                <FormGroup>
                                    <FormControl type="text" componentClass="input" placeholder="Nom d'utilisateur" value={this.state.user} onChange={this.handleUserChange}/>
                                </FormGroup>
                                <FormGroup>
                                    <FormControl componentClass="input" placeholder="Mot de passe" type="password" value={this.state.pass} onChange={this.handlePassChange}/>
                                </FormGroup>
                                <Button type="submit" bsSize="large" bsStyle="info" block>Se connecter</Button>
                            </fieldset>
                        </form>
                    </Panel>
                </Col>
            </Grid>
        )
    }
}

export default Login
