import { ADD_FLIGHTPICTURE_START, ADD_FLIGHTPICTURE_END, ADD_FLIGHTPICTURE_ERROR } from '../actions/FlightPicturesAction.js'
import { GET_FLIGHTPICTURES_START, GET_FLIGHTPICTURES_END, GET_FLIGHTPICTURES_ERROR } from '../actions/FlightPicturesAction.js'

const FlightPicturesInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        flightpictures: {},
    }
}

function FlightPicturesReducer(state = FlightPicturesInitialState, action) {
    switch (action.type) {
        /* ADD ARFIELD */
        case ADD_FLIGHTPICTURE_START:
            return {...state, requesting: true};
        case ADD_FLIGHTPICTURE_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    flightpictures: action.flightpictures
                }
            };
        case ADD_FLIGHTPICTURE_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    flightpictures: {}
                }
            };

        /* GET FLIGHTPICTURES */
        case GET_FLIGHTPICTURES_START:
            return {...state, requesting: true};
        case GET_FLIGHTPICTURES_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    flightpictures: action.flightpictures
                }
            };
        case GET_FLIGHTPICTURES_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    flightpictures: {}
                }
            };
        default:
            return state;
    }
}

export default FlightPicturesReducer