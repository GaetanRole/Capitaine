import { GET_STATS_START, GET_STATS_END, GET_STATS_ERROR } from '../actions/StatsAction.js'

const StatsInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        stats: {},
    }
}

function StatsReducer(state = StatsInitialState, action) {
    switch (action.type) {
        /* GET STATS */
        case GET_STATS_START:
            return {...state, requesting: true};
        case GET_STATS_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    stats: action.stats
                }
            };
        case GET_STATS_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    stats: {}
                }
            };
        default:
            return state;
    }
}

export default StatsReducer