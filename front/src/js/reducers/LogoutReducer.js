import { LOGOUT_START, LOGOUT_END } from '../actions/LogoutAction.js'

const LogoutInitialState = {
    requesting: false
}

function LogoutReducer(state = LogoutInitialState, action) {
    switch (action.type) {
        case LOGOUT_START:
            return {...state, requesting: true}
        case LOGOUT_END:
            return {...state, requesting: false}
        default:
            return state
    }
}

export default LogoutReducer