import { GET_PLANES_START, GET_PLANES_END, GET_PLANES_ERROR } from '../actions/PlanesAction.js'

const PlanesInitialState = {
    requesting: false,
    response: {
    	success: false,
    	error: false,
    	planes: {}
    }
}

function PlanesReducer(state = PlanesInitialState, action) {
    switch (action.type) {
        case GET_PLANES_START:
            return {...state, requesting: true}
        case GET_PLANES_END:
            return {...state, requesting: false, response: {
            		success: true,
            		error: false,
            		planes: action.planes,
            	}
            }
        case GET_PLANES_ERROR:
            return {...state, requesting: false, response: {
            		success: false,
            		error: true,
            		planes: {},
            	}
            }
        default:
            return state
    }
}

export default PlanesReducer