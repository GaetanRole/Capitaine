import { LOGIN_REQUEST_START, LOGIN_REQUEST_END, LOGIN_REQUEST_FAILED } from '../actions/LoginAction'

const LoginInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        infos: {}
    }
}

function LoginReducer(state = LoginInitialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST_START:
            return {...state, requesting: true}
        case LOGIN_REQUEST_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    infos: action.infos,
                }
            }
        case LOGIN_REQUEST_FAILED:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    infos: {},
                }
            }
        default:
            return state
    }
}

export default LoginReducer