import { GET_FLIGHTBYID_START, GET_FLIGHTBYID_END, GET_FLIGHTBYID_ERROR } from '../actions/FlightsAction.js'

const TrackingInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        flight: {},
    }
}

function TrackingReducer(state = TrackingInitialState, action) {
    switch (action.type) {
        /* GET FLIGHTBYID */
        case GET_FLIGHTBYID_START:
            return {...state, requesting: true};
        case GET_FLIGHTBYID_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    flight: action.flight
                }
            };
        case GET_FLIGHTBYID_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    flight: {}
                }
            };
        default:
            return state;
    }
}

export default TrackingReducer