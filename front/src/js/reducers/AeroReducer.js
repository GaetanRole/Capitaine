import { GET_METAR_START, GET_METAR_END, GET_METAR_ERROR } from '../actions/AeroAction.js'
import { GET_TAF_START, GET_TAF_END, GET_TAF_ERROR } from '../actions/AeroAction.js'
import { GET_NOTAM_START, GET_NOTAM_END, GET_NOTAM_ERROR } from '../actions/AeroAction.js'
import { GET_TEMSI_START, GET_TEMSI_END, GET_TEMSI_ERROR } from '../actions/AeroAction.js'
import { GET_WINTEM_START, GET_WINTEM_END, GET_WINTEM_ERROR } from '../actions/AeroAction.js'
import { GET_AIRFIELDS_START, GET_AIRFIELDS_END, GET_AIRFIELDS_ERROR } from '../actions/AirfieldsAction.js'

const AeroInitialState = {
    requestingAirfields: false,
    requestingInfo: false,
    responseAirfields: {
        success: false,
        error: false,
        data: [],
    },
    responseInfo: {
        success: false,
        error: false,
        data: '',
        wintem: '',
        temsi: '',
    }
}

function AeroReducer(state = AeroInitialState, action) {
    switch (action.type) {
        case GET_METAR_START:
            return {...state, requestingInfo: true};
        case GET_METAR_END:
            return {...state, requestingInfo: false, responseInfo: {
                    success: true,
                    error: false,
                    data: action.info,
                }
            };
        case GET_METAR_ERROR:
            return {...state, requestingInfo: false, responseInfo: {
                    error: true,
                    success: false,
                    data: '',
                }
            };
        case GET_TAF_START:
            return {...state, requestingInfo: true};
        case GET_TAF_END:
            return {...state, requestingInfo: false, responseInfo: {
                    success: true,
                    error: false,
                    data: action.info,
                }
            };
        case GET_TAF_ERROR:
            return {...state, requestingInfo: false, responseInfo: {
                    error: true,
                    success: false,
                    data: '',
                }
            };
        case GET_WINTEM_START:
            return {...state, requestingInfo: true};
        case GET_WINTEM_END:
            return {...state, requestingInfo: false, responseInfo: {
                    success: true,
                    error: false,
                    data: state.responseInfo.data,
                    wintem: action.info,
                    temsi: state.responseInfo.temsi,
                }
            };
        case GET_WINTEM_ERROR:
            return {...state, requestingInfo: false, responseInfo: {
                    error: true,
                    success: false,
                    data: state.responseInfo.data,
                    wintem: '',
                    temsi: state.responseInfo.temsi,
                }
            };
        case GET_TEMSI_START:
            return {...state, requestingInfo: true};
        case GET_TEMSI_END:
            return {...state, requestingInfo: false, responseInfo: {
                    success: true,
                    error: false,
                    data: state.responseInfo.data,
                    wintem: state.responseInfo.wintem,
                    temsi: action.temsi,
                }
            };
        case GET_TEMSI_ERROR:
            return {...state, requestingInfo: false, responseInfo: {
                    error: true,
                    success: false,
                    data: state.responseInfo.data,
                    wintem: state.responseInfo.wintem,
                    temsi: '',
                }
            };
        case GET_NOTAM_START:
            return {...state, requestingInfo: true};
        case GET_NOTAM_END:
            return {...state, requestingInfo: false, responseInfo: {
                    success: true,
                    error: false,
                    data: action.info,
                }
            };
        case GET_NOTAM_ERROR:
            return {...state, requestingInfo: false, responseInfo: {
                    error: true,
                    success: false,
                    data: '',
                }
            };
        case GET_AIRFIELDS_START:
            return {...state, requestingAirfields: true};
        case GET_AIRFIELDS_END:
            return {...state, requestingAirfields: false, responseAirfields: {
                    success: true,
                    error: false,
                    data: action.airfields,
                }
            };
        case GET_AIRFIELDS_ERROR:
            return {...state, requestingAirfields: false, responseAirfields: {
                    error: true,
                    success: false,
                    data: action.airfields,
                }
            };
        default:
            return state;
    }
}

export default AeroReducer