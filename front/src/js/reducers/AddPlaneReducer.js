import { ADD_PLANE_START, ADD_PLANE_END, ADD_PLANE_ERROR } from '../actions/AddPlaneAction.js'

const AddPlaneInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        planes: {},
    }
}

function AddPlaneReducer(state = AddPlaneInitialState, action) {
    switch (action.type) {
        case ADD_PLANE_START:
            return {...state, requesting: true};
        case ADD_PLANE_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    planes: action.planes
                }
            };
        case ADD_PLANE_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    planes: {}
                }
            };
        default:
            return state;
    }
}

export default AddPlaneReducer