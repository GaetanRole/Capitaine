import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import LoginReducer from './LoginReducer.js'
import LogoutReducer from './LogoutReducer.js'
import PlanesReducer from './PlanesReducer.js'
import AddPlaneReducer from './AddPlaneReducer.js'
import AirfieldsReducer from './AirfieldsReducer.js'
import RoutesReducer from './RoutesReducer.js'
import FlightPicturesReducer from './FlightPicturesReducer.js'
import FlightsReducer from './FlightsReducer.js'
import StatsReducer from './StatsReducer.js'
import TrackingReducer from './TrackingReducer.js'
import AeroReducer from './AeroReducer.js'

const rootReducer = combineReducers({
	routing: routerReducer,
	login: LoginReducer,
	logout: LogoutReducer,
	planes: PlanesReducer,
	addplane: AddPlaneReducer,
	airfields: AirfieldsReducer,
	routes: RoutesReducer,
	flightpictures: FlightPicturesReducer,
	flights: FlightsReducer,
	stats: StatsReducer,
	tracking: TrackingReducer,
	aero: AeroReducer,
})

export default rootReducer