import { ADD_FLIGHT_START, ADD_FLIGHT_END, ADD_FLIGHT_ERROR } from '../actions/FlightsAction.js'
import { GET_FLIGHTS_START, GET_FLIGHTS_END, GET_FLIGHTS_ERROR } from '../actions/FlightsAction.js'
const FlightsInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        flights: {},
    }
}

function FlightsReducer(state = FlightsInitialState, action) {
    switch (action.type) {
        /* ADD ARFIELD */
        case ADD_FLIGHT_START:
            return {...state, requesting: true};
        case ADD_FLIGHT_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    flights: action.flights
                }
            };
        case ADD_FLIGHT_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    flights: {}
                }
            };

        /* GET FLIGHTS */
        case GET_FLIGHTS_START:
            return {...state, requesting: true};
        case GET_FLIGHTS_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    flights: action.flights
                }
            };
        case GET_FLIGHTS_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    flights: {}
                }
            };
        default:
            return state;
    }
}

export default FlightsReducer