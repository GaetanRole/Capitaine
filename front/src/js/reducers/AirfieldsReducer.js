import { ADD_AIRFIELD_START, ADD_AIRFIELD_END, ADD_AIRFIELD_ERROR } from '../actions/AirfieldsAction.js'
import { GET_AIRFIELDS_START, GET_AIRFIELDS_END, GET_AIRFIELDS_ERROR } from '../actions/AirfieldsAction.js'

const AirfieldsInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        airfields: {},
    }
}

function AirfieldsReducer(state = AirfieldsInitialState, action) {
    switch (action.type) {
        /* ADD ARFIELD */
        case ADD_AIRFIELD_START:
            return {...state, requesting: true};
        case ADD_AIRFIELD_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    airfields: action.airfields
                }
            };
        case ADD_AIRFIELD_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    airfields: {}
                }
            };

        /* GET AIRFIELDS */
        case GET_AIRFIELDS_START:
            return {...state, requesting: true};
        case GET_AIRFIELDS_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    airfields: action.airfields
                }
            };
        case GET_AIRFIELDS_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    airfields: {}
                }
            };
        default:
            return state;
    }
}

export default AirfieldsReducer