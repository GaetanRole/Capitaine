import { ADD_ROUTE_START, ADD_ROUTE_END, ADD_ROUTE_ERROR } from '../actions/RoutesAction.js'
import { GET_ROUTES_START, GET_ROUTES_END, GET_ROUTES_ERROR } from '../actions/RoutesAction.js'

const RoutesInitialState = {
    requesting: false,
    response: {
        success: false,
        error: false,
        routes: {},
    }
}

function RoutesReducer(state = RoutesInitialState, action) {
    switch (action.type) {
        /* ADD ROUTE */
        case ADD_ROUTE_START:
            return {...state, requesting: true};
        case ADD_ROUTE_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    routes: action.routes
                }
            };
        case ADD_ROUTE_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    routes: {}
                }
            };

        /* GET ROUTES */
        case GET_ROUTES_START:
            return {...state, requesting: true};
        case GET_ROUTES_END:
            return {...state, requesting: false, response: {
                    success: true,
                    error: false,
                    routes: action.routes
                }
            };
        case GET_ROUTES_ERROR:
            return {...state, requesting: false, response: {
                    error: true,
                    success: false,
                    routes: {}
                }
            };
        default:
            return state;
    }
}

export default RoutesReducer