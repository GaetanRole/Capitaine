import { connect } from 'react-redux'
import Routes from '../presentationals/Routes.js'
import { getRoutes, addRoute } from '../actions/RoutesAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, routes: state.routes}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchAddRoute: ownProps => {
    		dispatch(addRoute(ownProps));
    	},
    	dispatchGetRoutes: ownProps => {
    		dispatch(getRoutes(ownProps));
    	}
    }
}

const RoutesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Routes)

export default RoutesContainer