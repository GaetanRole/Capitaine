import { connect } from 'react-redux'
import FlightPictures from '../presentationals/FlightPictures.js'
import { getFlightPictures, addFlightPicture } from '../actions/FlightPicturesAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, flightpictures: state.flightpictures}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchAddFlightPicture: ownProps => {
    		dispatch(addFlightPicture(ownProps));
    	},
    	dispatchGetFlightPictures: ownProps => {
    		dispatch(getFlightPictures(ownProps));
    	}
    }
}

const FlightPicturesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(FlightPictures)

export default FlightPicturesContainer