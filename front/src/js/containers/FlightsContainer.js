import { connect } from 'react-redux'
import Flights from '../presentationals/Flights.js'
import { getFlights, addFlight } from '../actions/FlightsAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, flights: state.flights}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchAddFlight: ownProps => {
    		dispatch(addFlight(ownProps));
    	},
    	dispatchGetFlights: ownProps => {
    		dispatch(getFlights(ownProps));
    	}
    }
}

const FlightsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Flights)

export default FlightsContainer