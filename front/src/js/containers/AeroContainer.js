import { connect } from 'react-redux'
import Aero from '../presentationals/Aero.js'
import { getMetarData, getTafData, getWintemData, getTemsiData, getNotamData } from '../actions/AeroAction.js'
import { getAirfields } from '../actions/AirfieldsAction.js';

const mapStateToProps = (state, ownProps) => {
    return {...state, aero: state.aero}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchGetMetarData: ownProps => {
    		dispatch(getMetarData(ownProps));
    	},
    	dispatchGetTafData: ownProps => {
    		dispatch(getTafData(ownProps));
    	},
    	dispatchGetNotamData: ownProps => {
    		dispatch(getNotamData(ownProps));
    	},
    	dispatchGetWintemData: ownProps => {
    		dispatch(getWintemData(ownProps));
    	},
    	dispatchGetTemsiData: ownProps => {
    		dispatch(getTemsiData(ownProps));
    	},
    	dispatchGetAirfields: ownProps => {
    		dispatch(getAirfields(ownProps));
    	}
    }
}

const AeroContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Aero)

export default AeroContainer