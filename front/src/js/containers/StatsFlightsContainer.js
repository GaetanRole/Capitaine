import { connect } from 'react-redux'
import StatsFlights from '../presentationals/StatsFlights.js'
import { getStats } from '../actions/StatsAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, statsflights: state.stats}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchGetStats: ownProps => {
    		dispatch(getStats(ownProps));
    	}
    }
}

const StatsFlightsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsFlights)

export default StatsFlightsContainer