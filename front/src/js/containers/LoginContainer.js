import { connect } from 'react-redux'
import Login from '../presentationals/Login.js'
import { getLoginToken } from '../actions/LoginAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, login : state.login}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatchGetLoginToken: (ownProps) => {
            dispatch(getLoginToken(ownProps))
        }
    }
}

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)

export default LoginContainer