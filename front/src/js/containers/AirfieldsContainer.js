import { connect } from 'react-redux'
import Airfields from '../presentationals/Airfields.js'
import { getAirfields, addAirfield } from '../actions/AirfieldsAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, airfields: state.airfields}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchAddAirfield: ownProps => {
    		dispatch(addAirfield(ownProps));
    	},
    	dispatchGetAirfields: ownProps => {
    		dispatch(getAirfields(ownProps));
    	}
    }
}

const AirfieldsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Airfields)

export default AirfieldsContainer