import { connect } from 'react-redux'
import Planes from '../presentationals/Planes.js'
import { getPlanes } from '../actions/PlanesAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, planes: state.planes}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchGetPlanes: ownProps => {
    		dispatch(getPlanes(ownProps));
    	}
    }
}

const PlanesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Planes)

export default PlanesContainer