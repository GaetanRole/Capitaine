import { connect } from 'react-redux'
import StatsFlightTime from '../presentationals/StatsFlightTime.js'
import { getStats } from '../actions/StatsAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, statsflighttime: state.stats}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchGetStats: ownProps => {
    		dispatch(getStats(ownProps));
    	}
    }
}

const StatsFlightTimeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsFlightTime)

export default StatsFlightTimeContainer