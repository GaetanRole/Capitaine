import { connect } from 'react-redux'
import StatsOther from '../presentationals/StatsOther.js'
import { getStats } from '../actions/StatsAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, statsother: state.stats}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchGetStats: ownProps => {
    		dispatch(getStats(ownProps));
    	}
    }
}

const StatsOtherContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsOther)

export default StatsOtherContainer