import { connect } from 'react-redux'
import Logout from '../presentationals/Logout.js'
import { logout } from '../actions/LogoutAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatchLogout: (ownProps) => {
            dispatch(logout(ownProps))
        }
    }
}

const LogoutContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Logout)

export default LogoutContainer