import { connect } from 'react-redux'
import Tracking from '../presentationals/Tracking.js'
import { getFlightById } from '../actions/FlightsAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, tracking: state.tracking}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchGetFlightById: ownProps => {
    		dispatch(getFlightById(ownProps));
    	}
    }
}

const TrackingContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Tracking)

export default TrackingContainer