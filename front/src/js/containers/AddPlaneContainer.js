import { connect } from 'react-redux'
import AddPlane from '../presentationals/AddPlane.js'
import { addPlane } from '../actions/AddPlaneAction.js'

const mapStateToProps = (state, ownProps) => {
    return {...state, addplane: state.addplane}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    	dispatchAddPlane: ownProps => {
    		dispatch(addPlane(ownProps));
    	}
    }
}

const AddPlaneContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AddPlane)

export default AddPlaneContainer